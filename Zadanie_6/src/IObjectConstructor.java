/**
 * Created by Pawel on 2016-01-03.
 */
public interface IObjectConstructor {
    boolean constructObjectFromRecipe(ConstructionRecipe recipe);
}
