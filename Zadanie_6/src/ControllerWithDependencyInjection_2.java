/**
 * Created by pawel on 05.01.2016.
 */
public class ControllerWithDependencyInjection_2 extends ControllerTemplateMethod {
    IObjectConstructor iObjectConstructor;
    IConstructionRecipeCreator iConstructionRecipeCreator;
    ILogger iLogger;
    IProductionLineMover iProductionLineMover;

    public ControllerWithDependencyInjection_2(IObjectConstructor iObjectConstructor,
                                               IConstructionRecipeCreator iConstructionRecipeCreator,
                                               ILogger iLogger, IProductionLineMover iProductionLineMover) {
        this.iObjectConstructor = iObjectConstructor;
        this.iConstructionRecipeCreator = iConstructionRecipeCreator;
        this.iLogger = iLogger;
        this.iProductionLineMover = iProductionLineMover;
    }

    void initialize() {
        iProductionLineMover.moveProductionLine(EndToEndTests.FORWARD);
        iLogger.log(EndToEndTests.WARNING, "log");
    }

    void work() {
        iConstructionRecipeCreator.numberOfElementsToProduce();
        iObjectConstructor.constructObjectFromRecipe(iConstructionRecipeCreator.constructionRecipe());

    }

    void end() {
        iLogger.log(EndToEndTests.ERROR, "log");
        iProductionLineMover.moveProductionLine(EndToEndTests.TOSCRAN);
    }
}
