import junit.framework.TestCase;

import java.util.Iterator;

/**
 * Created by Pawel on 2016-01-05.
 */
public class Tests extends TestCase {
    public void testDefaultConstructor() throws Exception {
        Matrix<Integer> matrix = new Matrix<>(Integer.class);

        assertEquals(1, matrix.getRows());
        assertEquals(1, matrix.getColumns());
        assertEquals(1, matrix.getTotalSize());
        assertEquals(null, matrix.at(0, 0));

        matrix.set(0, 0, 5);

        assertEquals(5, (int)matrix.at(0, 0));
    }

    public void testCustomSizeConstructor() throws Exception {
        int number_of_rows = 10;
        int number_of_columns = 25;

        Matrix<Integer> matrix = new Matrix<>(Integer.class, number_of_rows, number_of_columns);

        assertEquals(10, matrix.getRows());
        assertEquals(25, matrix.getColumns());
        assertEquals(250, matrix.getTotalSize());
    }

    public void testDefaultValueConstructorFloat() throws Exception {
        int number_of_rows = 4;
        int number_of_columns = 9;
        float default_value = 1.5f;

        Matrix<Float> matrix = new Matrix<>(Float.class, number_of_rows, number_of_columns, default_value);

        assertEquals(4, matrix.getRows());
        assertEquals(9, matrix.getColumns());
        assertEquals(4 * 9, matrix.getTotalSize());

        for (int i = 0; i < number_of_rows; i++) {
            for (int j = 0; j < number_of_columns; j++) {
                assertEquals(default_value, matrix.at(i, j));
            }
        }
    }

    public void testCopyConstructor() throws Exception {
        int number_of_rows = 2;
        int number_of_columns = 2;

        Matrix<Integer> matrix = new Matrix<>(Integer.class, number_of_rows, number_of_columns);

        matrix.set(0, 0, 1);
        matrix.set(0, 1, 2);
        matrix.set(1, 0, -2);
        matrix.set(1, 1, 5);

        Matrix<Integer> copiedMatrix = new Matrix<>(matrix);

        assertEquals(1, (int)copiedMatrix.at(0, 0));
        assertEquals(2, (int)copiedMatrix.at(0, 1));
        assertEquals(-2, (int)copiedMatrix.at(1, 0));
        assertEquals(5, (int)copiedMatrix.at(1, 1));
    }

    public void testDefaultValueConstructorChar() throws Exception {
        int number_of_rows = 4;
        int number_of_columns = 9;
        char default_value = 'a';

        Matrix<Character> matrix = new Matrix<>(Character.class, number_of_rows, number_of_columns, default_value);

        assertEquals(number_of_rows, matrix.getRows());
        assertEquals(number_of_columns, matrix.getColumns());
        assertEquals(number_of_rows * number_of_columns, matrix.getTotalSize());

        for (int i = 0; i < number_of_rows; i++) {
            for (int j = 0; j < number_of_columns; j++) {
                assertEquals(default_value, (char)matrix.at(i, j));
            }
        }
    }

    public void testRowCombining() throws Exception {
        int number_of_rows = 2;
        int number_of_columns = 2;

        Matrix<Integer> matrix = new Matrix<>(Integer.class, number_of_rows, number_of_columns);

        matrix.set(0, 0, 1);
        matrix.set(0, 1, 2);
        matrix.set(1, 0, -2);
        matrix.set(1, 1, 5);

        int rowA = 0, rowB = 1;

        matrix.combineRows(rowA, rowB);

        assertEquals(1, (int)matrix.at(0, 0));
        assertEquals(2, (int)matrix.at(0, 1));
        assertEquals(-1, (int)matrix.at(1, 0));
        assertEquals(7, (int)matrix.at(1, 1));

        matrix.combineRows(rowA, rowA);

        assertEquals(2, (int)matrix.at(0, 0));
        assertEquals(4, (int)matrix.at(0, 1));
        assertEquals(-1, (int)matrix.at(1, 0));
        assertEquals(7, (int)matrix.at(1, 1));
    }

    public void testColumnCombining() throws Exception {
        int number_of_rows = 2;
        int number_of_columns = 2;

        Matrix<Integer> matrix = new Matrix<>(Integer.class, number_of_rows, number_of_columns);

        matrix.set(0, 0, 1);
        matrix.set(0, 1, 2);
        matrix.set(1, 0, -2);
        matrix.set(1, 1, 5);

        int columnA = 0, columnB = 1;

        matrix.combineColumns(columnA, columnB);

        assertEquals(1, (int) matrix.at(0, 0));
        assertEquals(3, (int) matrix.at(0, 1));
        assertEquals(-2, (int) matrix.at(1, 0));
        assertEquals(3, (int) matrix.at(1, 1));

        matrix.combineColumns(columnA, columnA);

        assertEquals(2, (int) matrix.at(0, 0));
        assertEquals(3, (int) matrix.at(0, 1));
        assertEquals(-4, (int) matrix.at(1, 0));
        assertEquals(3, (int) matrix.at(1, 1));
    }

    public void testAppendingBoardsHorizontally() throws Exception {
        int number_of_rows = 1;
        int number_of_columns = 1;
        int default_value = 0;

        Matrix<Integer> matrix = new Matrix<>(Integer.class, number_of_rows, number_of_columns, default_value);
        matrix.set(0, 0, 1);

        int number_of_rows_1 = 2;
        int number_of_columns_1 = 1;
        int default_value_1 = 2;

        Matrix<Integer> appended_matrix = new Matrix<>(Integer.class, number_of_rows_1, number_of_columns_1, default_value_1);

        matrix.appendHorizontally(appended_matrix);

        assertEquals(2, matrix.getRows());
        assertEquals(2, matrix.getColumns());
        assertEquals(4, matrix.getTotalSize());
        assertEquals(1, (int) matrix.at(0, 0));
        assertEquals(2, (int) matrix.at(0, 1));
        assertEquals(0, (int) matrix.at(1, 0));
        assertEquals(2, (int) matrix.at(1, 1));

        int number_of_rows_2 = 1;
        int number_of_columns_2 = 2;
        int default_value_2 = 3;

        Matrix<Integer> appended_matrix_2 = new Matrix<>(Integer.class, number_of_rows_2, number_of_columns_2, default_value_2);

        matrix.appendHorizontally(appended_matrix_2);

        assertEquals(2, matrix.getRows());
        assertEquals(4, matrix.getColumns());
        assertEquals(8, matrix.getTotalSize());
        assertEquals(3, (int) matrix.at(0, 2));
        assertEquals(3, (int) matrix.at(0, 3));
        assertEquals(0, (int) matrix.at(1, 2));
        assertEquals(0, (int) matrix.at(1, 3));
    }

    public void testAppendingBoardsVertically() throws Exception {
        int number_of_rows = 1;
        int number_of_columns = 1;
        char default_value = '\0';

        Matrix<Character> matrix = new Matrix<>(Character.class, number_of_rows, number_of_columns, default_value);
        matrix.set(0, 0, 'a');

        int number_of_rows_1 = 1;
        int number_of_columns_1 = 2;
        char default_value_1 = 'b';

        Matrix<Character> appended_matrix = new Matrix<>(Character.class, number_of_rows_1, number_of_columns_1, default_value_1);

        matrix.appendVertically(appended_matrix);

        assertEquals(2, matrix.getRows());
        assertEquals(2, matrix.getColumns());
        assertEquals(4, matrix.getTotalSize());
        assertEquals('a', (char) matrix.at(0, 0));
        assertEquals('\0', (char) matrix.at(0, 1));
        assertEquals('b', (char) matrix.at(1, 0));
        assertEquals('b', (char) matrix.at(1, 1));

        int number_of_rows_2 = 2;
        int number_of_columns_2 = 1;
        char default_value_2 = 'c';

        Matrix<Character> appended_matrix_2 = new Matrix<>(Character.class, number_of_rows_2, number_of_columns_2, default_value_2);

        matrix.appendVertically(appended_matrix_2);

        assertEquals(4, matrix.getRows());
        assertEquals(2, matrix.getColumns());
        assertEquals(8, matrix.getTotalSize());
        assertEquals('a', (char) matrix.at(0, 0));
        assertEquals('\0', (char) matrix.at(0, 1));
        assertEquals('b', (char) matrix.at(1, 0));
        assertEquals('c', (char) matrix.at(2, 0));
        assertEquals('\0', (char) matrix.at(2, 1));
        assertEquals('c', (char) matrix.at(3, 0));
        assertEquals('\0', (char) matrix.at(3, 1));
    }

    public void testIterator() throws Exception {
        int number_of_rows = 3;
        int number_of_columns = 2;
        int default_value = 0;

        Matrix<Integer> matrix = new Matrix<>(Integer.class, number_of_rows, number_of_columns, default_value);

        matrix.set(0, 0, 1);
        matrix.set(0, 1, 2);
        matrix.set(1, 0, -2);
        matrix.set(1, 1, 5);
        matrix.set(2, 0, 6);
        matrix.set(2, 1, 6);

        Iterator iterator = matrix.iterator();

        assertEquals(1, iterator.next());
        assertEquals(2, iterator.next());
        assertEquals(-2, iterator.next());
        assertEquals(5, iterator.next());
        assertEquals(6, iterator.next());
        assertEquals(6, iterator.next());
        assertEquals(false, iterator.hasNext());

        int number_of_rows_1 = 1;
        int number_of_columns_1 = 1;
        Matrix<Integer> appended_matrix = new Matrix<>(Integer.class, number_of_rows_1, number_of_columns_1);
        appended_matrix.set(0, 0, 9);

        matrix.appendHorizontally(appended_matrix);

        Iterator iterator1 = matrix.iterator();

        assertEquals(1, iterator1.next());
        assertEquals(2, iterator1.next());
        assertEquals(9, iterator1.next());
        assertEquals(-2, iterator1.next());
        assertEquals(5, iterator1.next());
        assertEquals(0, iterator1.next());
        assertEquals(6, iterator1.next());
        assertEquals(6, iterator1.next());
        assertEquals(0, iterator1.next());
        assertEquals(false, iterator1.hasNext());
    }

    public void testIterator1() throws Exception {
        int number_of_rows = 3;
        int number_of_columns = 3;
        int default_value = 0;

        Matrix<Integer> matrix = new Matrix<>(Integer.class, number_of_rows, number_of_columns, default_value);

        matrix.set(0, 0, 1);
        matrix.set(0, 1, 2);
        matrix.set(1, 0, -2);
        matrix.set(1, 1, 5);
        matrix.set(1, 2, 8);
        matrix.set(2, 0, 6);
        matrix.set(2, 1, 6);

        Matrix.MatrixIterator iterator = (Matrix.MatrixIterator)matrix.iterator();

        assertEquals(1, iterator.next());
        assertEquals(0, iterator.goToRowEnd());
        assertEquals(-2, iterator.next());
        assertEquals(6, iterator.goToColumnEnd());
        assertEquals(0, iterator.goToEnd());
        assertEquals(1, iterator.goToBeginning());
        assertEquals(2, iterator.next());
    }

    public void testRowInserting() throws Exception {
        int number_of_rows = 1;
        int number_of_columns = 2;

        Matrix<Integer> matrix = new Matrix<>(Integer.class, number_of_rows, number_of_columns);
        matrix.set(0,0, 1);
        matrix.set(0,1, 2);

        Matrix.MatrixIterator iterator = (Matrix.MatrixIterator)matrix.iterator();
        iterator.next();

        assertEquals(2, iterator.next());

        iterator.insertRow(5);

        assertEquals(5, iterator.goToBeginning());
        assertEquals(5, iterator.next());
        assertEquals(1, iterator.next());
        assertEquals(2, iterator.next());
        assertEquals(false, iterator.hasNext());
        assertEquals(4, matrix.getTotalSize());

        iterator.goAfterEnd();
        iterator.insertRow(0);

        assertEquals(6, matrix.getTotalSize());
        assertEquals(0, (int)matrix.at(2, 0));
        assertEquals(0, (int)matrix.at(2, 1));
    }

    public void testColumnInserting() throws Exception {
        int number_of_rows = 2;
        int number_of_columns = 2;

        Matrix<Integer> matrix = new Matrix<>(Integer.class, number_of_rows, number_of_columns);
        matrix.set(0,0, 1);
        matrix.set(0,1, 2);
        matrix.set(1,0, 3);
        matrix.set(1,1, 4);

        Matrix.MatrixIterator iterator = (Matrix.MatrixIterator)matrix.iterator();

        assertEquals(1, iterator.next());

        iterator.insertColumn(9);

        assertEquals(9, iterator.goToBeginning());
        assertEquals(1, iterator.next());
        assertEquals(2, iterator.next());
        assertEquals(9, iterator.next());
        assertEquals(3, iterator.next());
        assertEquals(4, iterator.next());
        assertEquals(false, iterator.hasNext());
        assertEquals(6, matrix.getTotalSize());

        iterator.goAfterEnd();
        iterator.insertColumn(0);

        assertEquals(8, matrix.getTotalSize());
        assertEquals(0, (int)matrix.at(0, 3));
        assertEquals(0, (int)matrix.at(1, 3));
    }
}