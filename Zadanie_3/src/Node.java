/**
 * Created by Pawel on 2015-12-23.
 */
public class Node<T extends Comparable> {
    private T value;
    private Node<T> previous;
    private Node<T> middle;
    private Node<T> next;
    private int index;

    public Node(T value) {
        this.value = value;
    }

    public Node(T value, Node<T> previous) {
        this.value = value;
        this.previous = previous;
    }

    public T getValue() {
        return value;
    }

    public Node<T> getPrevious() {
        return previous;
    }

    public Node<T> getMiddle() {
        return middle;
    }

    public Node<T> getNext() {
        return next;
    }

    public int getIndex() {
        return index;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public void setPrevious(Node<T> previous) {
        this.previous = previous;
    }

    public void setMiddle(Node<T> middle) {
        this.middle = middle;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int compare(T value2) {
        return value.compareTo(value2);
    }

    public boolean checkVertical() {
        if(middle != null && previous == null && next == null) {
            return true;
        }
        return false;
    }

    public void clearConnections() {
        previous = null;
        next = null;
        middle = null;
    }
}
