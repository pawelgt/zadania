/**
 * Created by pawel on 01.12.2015.
 */
public class TextFactory implements Factory {
    private String text;
    private Object o;
    private Exporter exporter;
    private Importer importer;

    public TextFactory(String text) {
        this.text = text;
    }

    @Override
    public Object createData() {
        o = text;
        return o;
    }

    @Override
    public Exporter createExporter(Object o) {
        exporter = new TextExporter((String)o);
        return exporter;
    }

    @Override
    public Importer createImporter() {
        importer = new TextImporter();
        return importer;
    }
}
