/**
 * Created by pawel on 01.12.2015.
 */
public interface Factory {
    public Object createData();
    public Exporter createExporter(Object o);
    public Importer createImporter();
}
