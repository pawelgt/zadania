/**
 * Created by Pawel on 2016-01-02.
 */
public interface IProductionLineMover {
    int moveProductionLine(int direction);
}
