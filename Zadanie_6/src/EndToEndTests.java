import junit.framework.TestCase;
import org.easymock.EasyMock;

/**
 * Created by Pawel on 2016-01-03.
 */
public class EndToEndTests extends TestCase {
    IObjectConstructor objectConstructor;
    IConstructionRecipeCreator constructionRecipeCreator;
    ILogger logger;
    IProductionLineMover productionLineMover;
    ConstructionRecipe constructionRecipe;
    ControllerTemplateMethod controllerTemplateMethod;

    String nameOfObject = "UJ's first car";

    int numberOfObjectsToConstruct = 1;
    public static int FORWARD = 1, BACKWARDS = 2, TOSCRAN = 3;
    public static int ERROR = 1, WARNING = 2, INFO = 3;

    @Override
    public void setUp() {
        objectConstructor = EasyMock.createMock(IObjectConstructor.class);
        constructionRecipeCreator = EasyMock.createMock(IConstructionRecipeCreator.class);
        logger = EasyMock.createMock(ILogger.class);
        productionLineMover = EasyMock.createMock(IProductionLineMover.class);
        constructionRecipe = EasyMock.createMock(ConstructionRecipe.class);
        controllerTemplateMethod = new ControllerWithDependencyInjection(objectConstructor, constructionRecipeCreator, logger, productionLineMover);
    }

    public void testConstructionOfSingleObject() throws Exception {
        boolean wasSuccessful = true;

        EasyMock.expect(constructionRecipe.nameOfObject()).andReturn(nameOfObject);
        EasyMock.replay(constructionRecipe);

        EasyMock.expect(constructionRecipeCreator.constructionRecipe()).andReturn(constructionRecipe).times(1);
        EasyMock.expect(constructionRecipeCreator.numberOfElementsToProduce()).andReturn(numberOfObjectsToConstruct);
        EasyMock.replay(constructionRecipeCreator);

        EasyMock.expect(productionLineMover.moveProductionLine(EasyMock.eq(FORWARD))).andReturn(1).times(2);
        EasyMock.replay(productionLineMover);

        EasyMock.expect(objectConstructor.constructObjectFromRecipe(constructionRecipe)).andReturn(wasSuccessful).times(1);
        EasyMock.replay(objectConstructor);

        EasyMock.expect(logger.log(EasyMock.eq(INFO), EasyMock.anyString())).andReturn("Info").times(1);
        EasyMock.replay(logger);

        controllerTemplateMethod.execute();

        assertEquals(nameOfObject, constructionRecipe.nameOfObject());
        EasyMock.verify(constructionRecipeCreator);
        EasyMock.verify(productionLineMover);
        EasyMock.verify(objectConstructor);
        EasyMock.verify(logger);
    }

    public void testConstructionFailure_MovingProductionLineFailed() throws Exception {
        EasyMock.expect(constructionRecipe.nameOfObject()).andReturn(nameOfObject);
        EasyMock.replay(constructionRecipe);

        EasyMock.expect(constructionRecipeCreator.constructionRecipe()).andReturn(constructionRecipe).times(1);
        EasyMock.expect(constructionRecipeCreator.numberOfElementsToProduce()).andReturn(numberOfObjectsToConstruct);
        EasyMock.replay(constructionRecipeCreator);

        Exception exception = new RuntimeException("Can't move ProductionLine!");
        EasyMock.expect(productionLineMover.moveProductionLine(EasyMock.eq(FORWARD))).andThrow(exception).times(1);
        EasyMock.replay(productionLineMover);

        EasyMock.expect(logger.log(EasyMock.eq(ERROR), EasyMock.anyString())).andReturn("Error").times(1);
        EasyMock.replay(logger);

        try {
            controllerTemplateMethod.execute();
        }
        catch(RuntimeException e) {
            assertEquals(exception, e);
        }

        assertEquals(nameOfObject, constructionRecipe.nameOfObject());
        EasyMock.verify(productionLineMover);
    }

    public void testConstructionFailure_ObjectConstructionFailed() throws Exception {
        boolean wasSuccessful = true;

        EasyMock.expect(constructionRecipe.nameOfObject()).andReturn(nameOfObject);
        EasyMock.replay(constructionRecipe);

        EasyMock.expect(constructionRecipeCreator.constructionRecipe()).andReturn(constructionRecipe).times(1);
        EasyMock.expect(constructionRecipeCreator.numberOfElementsToProduce()).andReturn(numberOfObjectsToConstruct).times(1);
        EasyMock.replay(constructionRecipeCreator);

        EasyMock.expect(productionLineMover.moveProductionLine(EasyMock.eq(FORWARD))).andReturn(1).times(1);
        EasyMock.expect(productionLineMover.moveProductionLine(EasyMock.eq(TOSCRAN))).andReturn(1).times(1);
        EasyMock.replay(productionLineMover);

        EasyMock.expect(objectConstructor.constructObjectFromRecipe(constructionRecipe)).andReturn(wasSuccessful).times(1);
        EasyMock.replay(objectConstructor);

        EasyMock.expect(logger.log(EasyMock.eq(WARNING), EasyMock.anyString())).andReturn("Warning").times(1);
        EasyMock.replay(logger);

        //zmieniaja sie metody w execute->init,work,end, wiec tworze inna klase
        ControllerTemplateMethod controller_1 = new ControllerWithDependencyInjection_1(objectConstructor, constructionRecipeCreator, logger, productionLineMover);
        controller_1.execute();

        assertEquals(nameOfObject, constructionRecipe.nameOfObject());
        EasyMock.verify(constructionRecipeCreator);
        EasyMock.verify(objectConstructor);
        EasyMock.verify(logger);
        EasyMock.verify(productionLineMover);
    }

    public void testConstructionFailure_ObjectConstructionFailedThenMovingToScanAlsoFailed() throws Exception {
        boolean wasSuccessful = true;

        EasyMock.expect(constructionRecipe.nameOfObject()).andReturn(nameOfObject);
        EasyMock.replay(constructionRecipe);

        EasyMock.expect(constructionRecipeCreator.constructionRecipe()).andReturn(constructionRecipe).times(1);
        EasyMock.expect(constructionRecipeCreator.numberOfElementsToProduce()).andReturn(numberOfObjectsToConstruct).times(1);
        EasyMock.replay(constructionRecipeCreator);

        EasyMock.expect(objectConstructor.constructObjectFromRecipe(constructionRecipe)).andReturn(wasSuccessful).times(1);
        EasyMock.replay(objectConstructor);

        Exception exception = new RuntimeException("Can't move not constructed car to scan, scan is full!");
        EasyMock.expect(productionLineMover.moveProductionLine(EasyMock.eq(FORWARD))).andReturn(1).times(1);
        EasyMock.expect(productionLineMover.moveProductionLine(EasyMock.eq(TOSCRAN))).andThrow(exception);
        EasyMock.replay(productionLineMover);

        EasyMock.expect(logger.log(EasyMock.eq(WARNING), EasyMock.anyString())).andReturn("Warning").times(1);
        EasyMock.expect(logger.log(EasyMock.eq(ERROR), EasyMock.anyString())).andReturn("Error").times(1);
        EasyMock.replay(logger);

        try {
            ControllerTemplateMethod controller_2 = new ControllerWithDependencyInjection_2(objectConstructor, constructionRecipeCreator, logger, productionLineMover);
            controller_2.execute();
        }
        catch(RuntimeException e) {
            assertEquals(exception, e);
        }

        assertEquals(nameOfObject, constructionRecipe.nameOfObject());
        EasyMock.verify(constructionRecipeCreator);
        EasyMock.verify(objectConstructor);
        EasyMock.verify(logger);
        EasyMock.verify(productionLineMover);
    }
}