/**
 * Created by Pawel on 2016-01-02.
 */
public interface ILogger {
    String log(int type, String text);
}
