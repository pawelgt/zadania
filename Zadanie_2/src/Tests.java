import junit.framework.TestCase;

/**
 * Created by Pawel on 2015-12-22.
 */
public class Tests extends TestCase {
    public void testTree_BFS_DFS() throws Exception {
        Node<Integer> root = new Node<>(50);
        Node<Integer> node1 = new Node<>(30);
        Node<Integer> node2 = new Node<>(20);
        Node<Integer> node3 = new Node<>(40);
        Node<Integer> node4 = new Node<>(70);
        Node<Integer> node5 = new Node<>(60);
        Node<Integer> node6 = new Node<>(80);

        Tree<Integer> tree = new Tree<>(root);
        tree.addNode(node1);
        tree.addNode(node2);
        tree.addNode(node3);
        tree.addNode(node4);
        tree.addNode(node5);
        tree.addNode(node6);

        /*
              50
           /     \
          30      70
         /  \    /  \
        20   40  60   80 */

        String BFS = "50 30 70 20 40 60 80 ";
        String maybe_BFS = tree.printBFS();
        assertTrue(BFS.equals(maybe_BFS));

        String DFS = "50 30 20 40 70 60 80 ";
        String maybe_DFS = tree.printDFS();
        assertTrue(DFS.equals(maybe_DFS));
    }

    public void testTree_BFS_DFS_2() throws Exception {
        Node<Integer> root = new Node<>(50);
        Node<Integer> node1 = new Node<>(30);
        Node<Integer> node2 = new Node<>(20);
        Node<Integer> node3 = new Node<>(40);
        Node<Integer> node4 = new Node<>(70);
        Node<Integer> node5 = new Node<>(60);
        Node<Integer> node6 = new Node<>(80);
        Node<Integer> node7 = new Node<>(35);

        Tree<Integer> tree = new Tree<>(root);
        tree.addNode(node1);
        tree.addNode(node2);
        tree.addNode(node3);
        tree.addNode(node4);
        tree.addNode(node5);
        tree.addNode(node6);
        tree.addNode(node7);

        /*
              50
           /     \
          30      70
         /  \    /  \
        20   40  60   80
            /
           35 */

        String BFS = "50 30 70 20 40 60 80 35 ";
        String maybe_BFS = tree.printBFS();
        assertTrue(BFS.equals(maybe_BFS));

        Node root_2 = tree.findChild(node1);    //30
        Tree<Integer> tree_2 = new Tree<>(root_2);
        /*
          30
         /  \
        20   40
            /
           35 */


        String DFS = "30 20 40 35 ";
        String maybe_DFS = tree_2.printDFS();
        assertTrue(DFS.equals(maybe_DFS));
    }

    public void testTree_Delete() throws Exception {
        Node<Integer> root = new Node<>(50);
        Node<Integer> node1 = new Node<>(30);
        Node<Integer> node2 = new Node<>(20);
        Node<Integer> node3 = new Node<>(40);
        Node<Integer> node4 = new Node<>(70);
        Node<Integer> node5 = new Node<>(60);
        Node<Integer> node6 = new Node<>(80);

        Tree<Integer> tree = new Tree<>(root);
        tree.addNode(node1);
        tree.addNode(node2);
        tree.addNode(node3);
        tree.addNode(node4);
        tree.addNode(node5);
        tree.addNode(node6);

        /*
              50
           /     \
          30      70
         /  \    /  \
        20   40  60   80 */

        tree.delete(root);

        /*
              60
           /     \
          30      70
         /  \       \
        20   40      80 */

        String BFS = "60 30 70 20 40 80 ";
        String maybe_BFS = tree.printBFS();
        assertTrue(BFS.equals(maybe_BFS));

        tree.delete(node1);

        /*
              60
           /     \
          40      70
         /          \
        20           80 */

        String DFS = "60 40 20 70 80 ";
        String maybe_DFS = tree.printDFS();
        assertTrue(DFS.equals(maybe_DFS));
    }
    
    public void testTree_Double() {
        Node<Double> root = new Node<>(50.1);
        Node<Double> node1 = new Node<>(30.2);
        Node<Double> node2 = new Node<>(20.3);
        Node<Double> node3 = new Node<>(40.4);
        Node<Double> node4 = new Node<>(70.5);
        Node<Double> node5 = new Node<>(60.6);
        Node<Double> node6 = new Node<>(80.7);

        Tree<Double> tree = new Tree<>(root);
        tree.addNode(node1);
        tree.addNode(node2);
        tree.addNode(node3);
        tree.addNode(node4);
        tree.addNode(node5);
        tree.addNode(node6);

        /*
              50
           /     \
          30      70
         /  \    /  \
        20   40  60   80 */

        String BFS = "50.1 30.2 70.5 20.3 40.4 60.6 80.7 ";
        String maybe_BFS = tree.printBFS();
        assertTrue(BFS.equals(maybe_BFS));
    }
    
    public void testTree_String() {
        Node<String> root = new Node<>("cc");
        Node<String> node1 = new Node<>("bb");
        Node<String> node2 = new Node<>("aa");
        Node<String> node3 = new Node<>("ba");
        Node<String> node4 = new Node<>("ee");
        Node<String> node5 = new Node<>("dd");
        Node<String> node6 = new Node<>("ff");

        Tree<String> tree = new Tree<>(root);
        tree.addNode(node1);
        tree.addNode(node2);
        tree.addNode(node3);
        tree.addNode(node4);
        tree.addNode(node5);
        tree.addNode(node6);

        String BFS = "cc bb ee aa dd ff ba ";
        String maybe_BFS = tree.printBFS();
        assertTrue(BFS.equals(maybe_BFS));
    }
}