/**
 * Created by pawel on 05.01.2016.
 */
public abstract class ControllerTemplateMethod {
    abstract void initialize();
    abstract void work();
    abstract void end();

    public final void execute() {
        initialize();
        work();
        end();
    }
}
