import java.util.*;

/**
 * Created by Pawel on 2015-12-26.
 */
public class SortedDeque<T extends Comparable> implements Iterable<Node<T>> {
    List<Node<T>> list;
    private int size;
    private int unique_size;
    private int capacity;
    private int bucket_size;

    public SortedDeque() {
        this.list = new LinkedList<>();
        this.size = 0;
        this.unique_size = 0;
        this.capacity = Integer.MAX_VALUE;
        this.bucket_size = Integer.MAX_VALUE;
    }

    public SortedDeque(int bucket_size) {
        this.list = new LinkedList<>();
        this.size = 0;
        this.unique_size = 0;
        this.capacity = Integer.MAX_VALUE;
        this.bucket_size = bucket_size;
    }

    public void add(Node<T> node) {
        if(size < capacity) {
            boolean added = false;
            for (Node<T> n : list) {
                if (compare(n, node) == 0) {
                    if(n.getCount() < bucket_size) {
                        n.incrementCount();
                        size++;
                        added = true;
                    }
                }
            }

            if (!added) {
                list.add(node);
                sortList();
                size++;
                unique_size++;
            }
        }
    }

    public void add(T value) {
        if(size < capacity) {
            Node<T> node = new Node<>(value);
            boolean added = false;
            for (Node<T> n : list) {
                if (compare(n, node) == 0) {
                    if(n.getCount() < bucket_size) {
                        n.incrementCount();
                        size++;
                        added = true;
                    }
                    else {
                        added = true;
                    }
                }
            }

            if (!added) {
                list.add(node);
                sortList();
                size++;
                unique_size++;
            }
        }
    }

    private void sortList() {
        Collections.sort(list, new NodeComparator());
    }

    private int compare(Node<T> node1, Node<T> node2) {
        return node1.compare(node2.getValue());
    }

    class NodeComparator implements Comparator<Node<T>> {
        @Override
        public int compare(Node<T> a, Node<T> b) {
            return a.getValue().compareTo(b.getValue());
        }
    }

    public int getSize() {
        return size;
    }

    public int getUnique_size() {
        return unique_size;
    }

    public int getCapacity() {
        return capacity;
    }

    public void printDeque() {
        for (Node<T> n : list) {
            System.out.println("Value: '" + n.getValue() + "' count: " + n.getCount());
        }
    }

    public Node<T> get(T value) {
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getValue() == value) {
                return list.get(i);
            }
        }
        return null;
    }

    public Node<T> get(int i) {
        return list.get(i);
    }

    public void reserve(int capacity) {
        this.capacity = capacity;
    }

    public boolean isEmpty() {
        return list.size() == 0;
    }

    public Iterator<Node<T>> iterator() {
        return new NodeIterator();
    }

    class NodeIterator implements Iterator<Node<T>> {
        private int index = 0;

        public boolean hasNext() {
            return index < size;
        }

        public Node<T> next() {
            Node<T> node = list.get(index++);

            return node;
        }

        public void remove() {
            throw new UnsupportedOperationException("not supported yet");
        }
    }
}
