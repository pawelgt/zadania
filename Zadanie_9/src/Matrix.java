import java.lang.reflect.Array;
import java.util.Iterator;

/**
 * Created by Pawel on 2016-01-05.
 */
public class Matrix<T> implements Iterable<T> {
    T[][] tab;
    private int rows;
    private int columns;
    private T defaultValue;
    private Class<T> c;

    public Matrix(Class<T> c) {
        this.rows = 1;
        this.columns = 1;
        this.c = c;
        tab = (T[][]) Array.newInstance(c, rows, columns);
    }

    public Matrix(Class<T> c, int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.c = c;
        tab = (T[][]) Array.newInstance(c, rows, columns);
    }

    public Matrix(Class<T> c, int rows, int columns, T defaultValue) {
        this.rows = rows;
        this.columns = columns;
        this.defaultValue = defaultValue;
        this.c = c;
        tab = (T[][]) Array.newInstance(c, rows, columns);
    }

    public Matrix(Matrix matrix) {
        this.rows = matrix.getRows();
        this.columns = matrix.getColumns();
        this.defaultValue = (T)matrix.getDefaultValue();
        this.tab = (T[][])matrix.getTab();
        this.c = matrix.getC();
    }

    public T at(int row, int column) {
        if(defaultValue != null) {
            if(tab[row][column] == null) {
                return defaultValue;
            }
            else {
                return tab[row][column];
            }
        }
        else {
            return tab[row][column];
        }
    }

    public void set(int row, int column, T value) {
        tab[row][column] = value;
    }

    public void combineRows(int rowA, int rowB) {
        //w Javie nie da sie uzyc operatora + dla generykow
        if(tab[0][0] instanceof Integer) {
            for (int i = 0; i < columns; i++) {
                Integer tmp = (Integer)tab[rowB][i] + (Integer)tab[rowA][i];
                tab[rowB][i] = (T)tmp;
            }
        }
        else if(tab[0][0] instanceof String) {
            for (int i = 0; i < columns; i++) {
                String tmp = (String)tab[rowB][i] + (String)tab[rowA][i];
                tab[rowB][i] = (T)tmp;
            }
        }
        else if(tab[0][0] instanceof Double) {
            for (int i = 0; i < columns; i++) {
                Double tmp = (Double)tab[rowB][i] + (Double)tab[rowA][i];
                tab[rowB][i] = (T)tmp;
            }
        }
    }
    public void combineColumns(int columnA, int columnB) {
        //w Javie nie da sie uzyc operatora + dla generykow
        if(tab[0][0] instanceof Integer) {
            for (int i = 0; i < columns; i++) {
                Integer tmp = (Integer)tab[i][columnB] + (Integer)tab[i][columnA];
                tab[i][columnB] = (T)tmp;
            }
        }
        else if(tab[0][0] instanceof String) {
            for (int i = 0; i < columns; i++) {
                String tmp = (String)tab[i][columnB] + (String)tab[i][columnA];
                tab[i][columnB] = (T)tmp;
            }
        }
        else if(tab[0][0] instanceof Double) {
            for (int i = 0; i < columns; i++) {
                Double tmp = (Double)tab[i][columnB] + (Double)tab[i][columnA];
                tab[i][columnB] = (T)tmp;
            }
        }
    }

    public void appendHorizontally(Matrix<T> matrix) {
        T[][] tab1 = (T[][]) Array.newInstance(c, Math.max(rows, matrix.getRows()), columns + matrix.getColumns());

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                tab1[i][j] = tab[i][j];
            }
        }

        for (int i = 0; i < matrix.getRows(); i++) {
            for (int j = 0; j < matrix.getColumns(); j++) {
                tab1[i][columns + j] = matrix.at(i, j);
            }
        }

        tab = tab1;
        rows = tab.length;
        columns = tab[0].length;
    }

    public void appendVertically(Matrix<T> matrix) {
        T[][] tab1 = (T[][]) Array.newInstance(c, rows + matrix.getRows(), Math.max(columns, matrix.getColumns()));

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                tab1[i][j] = tab[i][j];
            }
        }

        for (int i = 0; i < matrix.getRows(); i++) {
            for (int j = 0; j < matrix.getColumns(); j++) {
                tab1[rows + i][j] = matrix.at(i, j);
            }
        }

        tab = tab1;
        rows = tab.length;
        columns = tab[0].length;
    }

    public void printMatrix() {
        System.out.println();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                System.out.print(at(i, j) + " ");
            }
            System.out.println();
        }
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public int getTotalSize() {
        return rows * columns;
    }

    public T getDefaultValue() {
        return defaultValue;
    }

    public T[][] getTab() {
        return tab;
    }

    public Class<T> getC() {
        return c;
    }

    private void insertRowP(int ins_row, T value) {
        T[][] tab1 = (T[][]) Array.newInstance(c, rows + 1, columns);

        for (int i = 0; i < ins_row; i++) {
            for (int j = 0; j < columns; j++) {
                tab1[i][j] = tab[i][j];
            }
        }

        for (int i = 0; i < columns; i++) {
            tab1[ins_row][i] = value;
        }

        for (int i = ins_row; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                tab1[i + 1][j] = tab[i][j];
            }
        }

        rows++;
        tab = tab1;
    }

    private void insertColumnP(int ins_col, T value) {
        T[][] tab1 = (T[][]) Array.newInstance(c, rows, columns + 1);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < ins_col; j++) {
                tab1[i][j] = tab[i][j];
            }
        }

        for (int i = 0; i < rows; i++) {
            tab1[i][ins_col] = value;
        }

        for (int i = 0; i < rows; i++) {
            for (int j = ins_col; j < columns; j++) {
                tab1[i][j + 1] = tab[i][j];
            }
        }

        columns++;
        tab = tab1;
    }

    public Iterator<T> iterator() {
        return new MatrixIterator();
    }

    class MatrixIterator implements Iterator<T> {
        int it_row = 0;
        int it_col = 0;
        boolean end = false;

        public boolean hasNext() {
            return !(it_col == columns || it_row == rows || end);
        }

        public T next() {
            if(it_col == 0 && it_row ==0) {
                it_col++;
                return at(0, 0);
            }
            if(it_col == columns - 1) {
                it_col = 0;
                it_row++;
                return at(it_row - 1, columns - 1);
            }
            else {
                it_col++;
                return at(it_row, it_col-1);
            }
        }

        public T goToEnd() {
            end = false;
            it_col = columns - 1;
            it_row = rows - 1;
            return next();
        }

        public T goToBeginning() {
            end = false;
            it_col = 0;
            it_row = 0;
            return next();
        }

        public T goToRowEnd() {
            end = false;
            it_col = columns - 1;
            return next();
        }

        public T goToColumnEnd() {
            end = false;
            it_row = rows - 1;
            return next();
        }

        public T goAfterEnd() {
            end = true;
            return null;
        }

        public void insertRow(T value) {
            if(end) {
                insertRowP(columns, value);
            }
            else if(it_col == 0 && it_row > 0) {
                insertRowP(it_row - 1, value);
            }
            else {
                insertRowP(it_row, value);
            }
        }

        public void insertColumn(T value) {
            if(end) {
                insertColumnP(columns, value);
            }
            else if(it_col == 0 && it_row == 0) {
                insertColumnP(0, value);
            }
            else if(it_col == 0) {
                insertColumnP(columns - 1, value);
            }
            else {
                insertColumnP(it_col - 1, value);
            }
        }
    }
}
