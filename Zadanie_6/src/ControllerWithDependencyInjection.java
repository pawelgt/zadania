/**
 * Created by pawel on 05.01.2016.
 */
public class ControllerWithDependencyInjection extends ControllerTemplateMethod {
    IObjectConstructor iObjectConstructor;
    IConstructionRecipeCreator iConstructionRecipeCreator;
    ILogger iLogger;
    IProductionLineMover iProductionLineMover;

    public ControllerWithDependencyInjection(IObjectConstructor iObjectConstructor,
                                             IConstructionRecipeCreator iConstructionRecipeCreator,
                                             ILogger iLogger, IProductionLineMover iProductionLineMover) {
        this.iObjectConstructor = iObjectConstructor;
        this.iConstructionRecipeCreator = iConstructionRecipeCreator;
        this.iLogger = iLogger;
        this.iProductionLineMover = iProductionLineMover;
    }

    void initialize() {
        iProductionLineMover.moveProductionLine(EndToEndTests.FORWARD);
    }

    void work() {
        iConstructionRecipeCreator.numberOfElementsToProduce();
        iProductionLineMover.moveProductionLine(EndToEndTests.FORWARD);
        iObjectConstructor.constructObjectFromRecipe(iConstructionRecipeCreator.constructionRecipe());
    }

    void end() {
        iLogger.log(EndToEndTests.INFO, "log");
    }
}
