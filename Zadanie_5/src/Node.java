/**
 * Created by Pawel on 2015-12-26.
 */
public class Node<T extends Comparable> {
    private T value;
    private int count;

    public Node(T value) {
        this.value = value;
        this.count = 1;
    }

    public T getValue() {
        return value;
    }

    public int compare(T value2) {
        return value.compareTo(value2);
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void incrementCount() {
        this.count++;
    }
}
