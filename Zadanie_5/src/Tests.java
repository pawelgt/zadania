import junit.framework.TestCase;

import java.util.Random;

/**
 * Created by Pawel on 2015-12-26.
 */
public class Tests extends TestCase {

    public void testConstructionOfEmptyContainer() throws Exception {
        SortedDeque<Integer> deque = new SortedDeque<>();

        assertEquals(0, deque.getSize());
        assertEquals(0, deque.getUnique_size());
        assertTrue(deque.isEmpty());
    }

    public void testDequeSizeRandom() throws Exception {
        int size_of_bucket = 3;
        int max_value = 50;
        SortedDeque<Integer> deque = new SortedDeque<>(size_of_bucket);
        deque.reserve(100);

        fill(deque, max_value);

        for (int i = 0; i < deque.getUnique_size(); i++) {
            Node<Integer> node = deque.get(i);
            assertTrue(node.getCount() <= size_of_bucket);
        }
        assertTrue(deque.getUnique_size() <= max_value);
    }

    public void testDequeSizeFull() throws Exception {
        int size_of_bucket = 3;
        int max_value = 20;
        SortedDeque<Integer> deque = new SortedDeque<>(size_of_bucket);
        deque.reserve(size_of_bucket * max_value);

        for (int i = 0; i < max_value; i++) {
            for (int j = 0; j < size_of_bucket; j++) {
                deque.add(i);
            }
        }
        assertEquals(max_value, deque.getUnique_size());

        for (int i = 0; i < deque.getUnique_size(); i++) {
            Node<Integer> node = deque.get(i);
            assertEquals(3, node.getCount());
            assertEquals(i, (int)node.getValue());
        }
    }

    public void testDeque_1() throws Exception {
        SortedDeque<Integer> deque = new SortedDeque<>();

        deque.add(2);
        deque.add(1);
        deque.add(3);
        deque.add(1);
        deque.add(3);

        assertEquals(5, deque.getSize());
        assertEquals(3, deque.getUnique_size());

        //testing count
        assertEquals(2, (deque.get((Integer)1).getCount()));
        assertEquals(1, (deque.get((Integer)2).getCount()));
        assertEquals(2, (deque.get((Integer)3).getCount()));

        //testing order
        assertEquals(1, (int)deque.get(0).getValue());
        assertEquals(2, (int)deque.get(1).getValue());
        assertEquals(3, (int)deque.get(2).getValue());
    }

    public void testDeque_2() throws Exception {
        SortedDeque<String> deque = new SortedDeque<>();

        deque.add("c");
        deque.add("d");
        deque.add("f");
        deque.add("f");
        deque.add("c");
        deque.add("z");

        assertEquals(6, deque.getSize());
        assertEquals(4, deque.getUnique_size());

        //testing count
        assertEquals(2, (deque.get("c").getCount()));
        assertEquals(1, (deque.get("d").getCount()));
        assertEquals(2, (deque.get("f").getCount()));
        assertEquals(1, (deque.get("z").getCount()));

        //testing order
        assertEquals("c", deque.get(0).getValue());
        assertEquals("d", deque.get(1).getValue());
        assertEquals("f", deque.get(2).getValue());
        assertEquals("z", deque.get(3).getValue());
    }

    public void testDeque_3() throws Exception {
        SortedDeque<Integer> deque = new SortedDeque<>();

        deque.add(2);
        deque.add(1);
        deque.add(3);
        deque.add(1);
        deque.add(3);

        assertEquals(5, deque.getSize());
        assertEquals(3, deque.getUnique_size());

        //testing count
        assertEquals(2, (deque.get((Integer)1).getCount()));
        assertEquals(1, (deque.get((Integer)2).getCount()));
        assertEquals(2, (deque.get((Integer)3).getCount()));

        //testing order
        assertEquals(1, (int)deque.get(0).getValue());
        assertEquals(2, (int)deque.get(1).getValue());
        assertEquals(3, (int)deque.get(2).getValue());

        //add 0
        deque.add(0);

        assertEquals(6, deque.getSize());
        assertEquals(4, deque.getUnique_size());

        assertEquals(1, (deque.get((Integer)0).getCount()));
        assertEquals(2, (deque.get((Integer)1).getCount()));
        assertEquals(1, (deque.get((Integer)2).getCount()));
        assertEquals(2, (deque.get((Integer)3).getCount()));

        assertEquals(0, (int)deque.get(0).getValue());
        assertEquals(1, (int)deque.get(1).getValue());
        assertEquals(2, (int)deque.get(2).getValue());
        assertEquals(3, (int)deque.get(3).getValue());
    }

    public void testDequeOverSize() throws Exception {
        int size_of_bucket = 2;
        SortedDeque<Integer> deque = new SortedDeque<>(size_of_bucket);

        deque.add(5);
        deque.add(2);
        deque.add(1);
        deque.add(1);
        deque.add(1);
        deque.add(5);

        assertEquals(5, deque.getSize());
        assertEquals(2, deque.get((Integer)1).getCount());
        assertEquals(2, deque.get((Integer)5).getCount());
        assertEquals(1, deque.get((Integer)2).getCount());
    }

    public void testIterator() throws Exception {
        float[] values = {1.11f, 2.22f, 3.33f, 4.44f, 5.55f };
        SortedDeque<Float> deque = new SortedDeque<>();

        for (int i = 0; i < values.length; i++) {
            deque.add(values[i]);
        }

        int index = 0;
        for (Node<Float> node : deque) {
            assertEquals(values[index++], node.getValue());
        }
    }

    public void fill(SortedDeque deque, int b) {
        Random generator = new Random();
        while(deque.getSize() < deque.getCapacity()) {
            deque.add(generator.nextInt(b));
        }
    }

}