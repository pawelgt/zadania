import junit.framework.TestCase;
import org.easymock.EasyMock;

/**
 * Created by Pawel on 2016-01-02.
 */
public class SingleComponentTester extends TestCase{

    public void testILogger() throws Exception {
        int ERROR = 1, WARNING = 2, INFO = 3;

        ILogger iLogger = EasyMock.createMock(ILogger.class);

        EasyMock.expect(iLogger.log(EasyMock.eq(ERROR), EasyMock.anyString())).andReturn("Error").times(2);
        EasyMock.expect(iLogger.log(EasyMock.eq(WARNING), EasyMock.anyString())).andReturn("Warning").times(1);
        EasyMock.expect(iLogger.log(EasyMock.eq(INFO), EasyMock.anyString())).andReturn("Info").times(3);

        EasyMock.replay(iLogger);

        iLogger.log(ERROR, "seriousError");
        iLogger.log(ERROR, "seriousError1");
        iLogger.log(WARNING, "Warning");
        iLogger.log(INFO, "Done");
        iLogger.log(INFO, "Done1");
        iLogger.log(INFO, "Done2");

        EasyMock.verify(iLogger);
    }

    public void testProductionLineMover() throws Exception {
        int FORWARD = 1, BACKWARDS = 2, TOSCRAN = 3;

        IProductionLineMover iProductionLineMover = EasyMock.createMock((IProductionLineMover.class));

        EasyMock.expect(iProductionLineMover.moveProductionLine(EasyMock.eq(FORWARD))).andReturn(1).times(3);
        EasyMock.expect(iProductionLineMover.moveProductionLine(EasyMock.eq(TOSCRAN))).andReturn(3).times(1);

        EasyMock.replay(iProductionLineMover);

        iProductionLineMover.moveProductionLine(FORWARD);
        iProductionLineMover.moveProductionLine(TOSCRAN);
        iProductionLineMover.moveProductionLine(FORWARD);
        iProductionLineMover.moveProductionLine(FORWARD);

        EasyMock.verify(iProductionLineMover);
    }

    public void testConstructionRecipe() throws Exception {
        String nameOfObject = "Fiat 126p";

        ConstructionRecipe constructionRecipe = EasyMock.createMock(ConstructionRecipe.class);

        EasyMock.expect(constructionRecipe.nameOfObject()).andReturn(nameOfObject);
        EasyMock.replay(constructionRecipe);

        assertEquals(nameOfObject, constructionRecipe.nameOfObject());
    }

    public void testIConstructionRecipe() throws Exception {
        String nameOfObject = "Fiat 126p";

        ConstructionRecipe mock = EasyMock.createMock(ConstructionRecipe.class);

        EasyMock.expect(mock.nameOfObject()).andReturn(nameOfObject);
        EasyMock.replay(mock);

        int numberOfObjectsToProduce = 10;
        IConstructionRecipeCreator mockCreator = EasyMock.createMock(IConstructionRecipeCreator.class);
        EasyMock.expect(mockCreator.constructionRecipe()).andReturn(mock);
        EasyMock.expect(mockCreator.numberOfElementsToProduce()).andReturn(numberOfObjectsToProduce);
        EasyMock.replay(mockCreator);

        assertEquals(numberOfObjectsToProduce, mockCreator.numberOfElementsToProduce());
        assertEquals(nameOfObject, mockCreator.constructionRecipe().nameOfObject());
    }

    public void testObjectConstructor() throws Exception {
        ConstructionRecipe mock = EasyMock.createMock(ConstructionRecipe.class);
        IObjectConstructor mockObjectConstructor = EasyMock.createMock(IObjectConstructor.class);

        EasyMock.expect(mockObjectConstructor.constructObjectFromRecipe(mock)).andReturn(true).times(1);

        EasyMock.replay(mock);
        EasyMock.replay(mockObjectConstructor);

        assertEquals(true, mockObjectConstructor.constructObjectFromRecipe(mock));
        EasyMock.verify(mockObjectConstructor);
    }
}