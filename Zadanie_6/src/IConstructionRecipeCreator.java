/**
 * Created by Pawel on 2016-01-03.
 */
public interface IConstructionRecipeCreator {
    ConstructionRecipe constructionRecipe();
    int numberOfElementsToProduce();
}
